package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            return rawAge*2000;
        } else if (rawAge <50) {
            return rawAge*2250;
        } else {
            return rawAge*5000;
        }
    }
    public String countPowerClass(int birthYear) {
        int rawPower = countPowerPotensialFromBirthYear(birthYear);
        String myClass = "";
        if (rawPower>=0 && rawPower<=20000){
            myClass = "C class";
        }else if (rawPower>20000 && rawPower<=100000) {
            myClass = "B class";
        }else {
            myClass = "A class";
        }
        return myClass;
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
