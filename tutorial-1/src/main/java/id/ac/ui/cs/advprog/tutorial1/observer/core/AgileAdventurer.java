package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
                //ToDo: Complete Me
        }

        //ToDo: Complete Me
        @Override
        public void update(){
                String type = this.guild.getQuestType();
                if (type.equals("D") || type.equals("R")){
                        this.getQuests().add(this.guild.getQuest());
                }

        }
}
