package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                //ToDo: Complete Me
        }

        //ToDo: Complete Me
        @Override
        public void update(){
                String type = this.guild.getQuestType();
                if (type.equals("D") || type.equals("E")){
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
