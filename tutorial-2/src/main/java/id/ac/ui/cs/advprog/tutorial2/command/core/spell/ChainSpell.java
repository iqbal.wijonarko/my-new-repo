package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> ListOfSpell;

    public ChainSpell(ArrayList<Spell> ListOfSpell){
        this.ListOfSpell = ListOfSpell;
    }

    @Override
    public void cast() {
        for (Spell spell : ListOfSpell){
            spell.cast();
        }
    }

    @Override
    public void undo(){
        for (int i = ListOfSpell.size()-1 ; i>=0 ; i--){
            ListOfSpell.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
