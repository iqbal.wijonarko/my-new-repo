package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me

        int jumlahChild = guild.getMemberList().size();
        Member iqbal = new PremiumMember("iqbalPremium","Master");
        guild.addMember(guildMaster, iqbal);
        Member getMember = guild.getMember("iqbalPremium","Master");

        assertEquals(jumlahChild + 1, guild.getMemberList().size());
        assertEquals(iqbal,getMember);
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member iqbal = new PremiumMember("iqbalPremium", "Master");
        guild.addMember(guildMaster,iqbal);

        int jumlahChild = guild.getMemberList().size();
        guild.removeMember(guildMaster,iqbal);
        Member getMember = guild.getMember("iqbalPremium","Master");

        assertEquals(jumlahChild-1, guild.getMemberList().size());
        assertEquals(null,getMember);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member iqbal = new PremiumMember("iqbal","Master");
        guild.addMember(guildMaster,iqbal);
        Member getMember = guild.getMember("iqbal","Master");

        assertEquals(iqbal,getMember);
    }
}
