package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati",member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant",member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member member2 = new PremiumMember("premium2", "role2");
        int jumlahChild = member.getChildMembers().size();
        member.addChildMember(member2);
        assertEquals(jumlahChild + 1, member.getChildMembers().size());

        Member member3 = new OrdinaryMember("ordinary3", "role3");
        jumlahChild = member.getChildMembers().size();
        member.addChildMember(member3);

        assertEquals(jumlahChild + 1, member.getChildMembers().size());

    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member member3 = new PremiumMember("premium3", "role3");
        int sebelumAdd = member.getChildMembers().size();
        member.addChildMember(member3);
        assertEquals(sebelumAdd + 1, member.getChildMembers().size());

        int sebelumRemove = member.getChildMembers().size();
        member.removeChildMember(member3);

        assertEquals(sebelumRemove - 1, member.getChildMembers().size());

    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member member4 = new PremiumMember("premium4", "Non-Master");
        Member member5 = new PremiumMember("premium5", "Non-Master");
        Member member6 = new PremiumMember("premium6", "Non-Master");
        Member member7 = new PremiumMember("premium7", "Non-Master");
        Member member8 = new PremiumMember("premium8", "Non-Master");

        member4.addChildMember(member5);
        assertEquals(1, member4.getChildMembers().size());

        member4.addChildMember(member6);
        assertEquals(2, member4.getChildMembers().size());

        member4.addChildMember(member7);
        assertEquals(3, member4.getChildMembers().size());

        member4.addChildMember(member8);
        assertEquals(3, member4.getChildMembers().size());

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("premium master", "Master");
        Member member5 = new PremiumMember("premium5", "Master");
        Member member6 = new PremiumMember("premium6", "Master");
        Member member7 = new PremiumMember("premium7", "Master");
        Member member8 = new PremiumMember("premium8", "Master");

        master.addChildMember(member5);
        assertEquals(1, master.getChildMembers().size());

        master.addChildMember(member6);
        assertEquals(2, master.getChildMembers().size());

        master.addChildMember(member7);
        assertEquals(3, master.getChildMembers().size());

        master.addChildMember(member8);
        assertEquals(4, master.getChildMembers().size());

    }
}
