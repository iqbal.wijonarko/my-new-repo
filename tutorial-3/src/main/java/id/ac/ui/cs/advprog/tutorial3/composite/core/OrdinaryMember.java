package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
        //TODO: Complete me
    private String name;
    private String role;

    public OrdinaryMember(String name, String role){
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        // do nothing
    }

    @Override
    public void removeChildMember(Member member) {
        // do nothing
    }

    @Override
    public List<Member> getChildMembers() {
        // empty list
        return new ArrayList<>();
    }
}

