package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    RandomClass random;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
        random = new RandomClass(5,10);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return random.generate() + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Raw " +weapon.getDescription();
    }
}
